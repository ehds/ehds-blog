---
title: 整数的2次幂分解
description: 给定一个正整数N，找到所有的可能解使得这些数字之和等于N
date: '2018-10-10T02:46:50.792Z'
published: false
cover: ./algorithm.png
coverAuthor: unknown
coverOriginalUrl: https://miro.medium.com/max/1105/0*gy5hAiFcQydBAa4K.png
---

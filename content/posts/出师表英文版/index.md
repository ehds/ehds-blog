---
title: 出师表英文版
description: 军师联盟诸葛亮扮演者王洛勇用英文朗诵出师表。
date: '2017-12-27T21:12:29.792Z'
published: true
cover: ../images/default.jpg
coverAuthor: Mark de Jong
coverOriginalUrl: https://storage.googleapis.com/gweb-cloudblog-publish/original_images/Google_Cloud_Networking.jpg
---

军师联盟诸葛亮扮演者王洛勇用英文朗诵出师表。



[B站地址](https://www.bilibili.com/video/av17671902/?from=search&seid=16394760502509107485)

下面附上原文:

*注：素材来源于网络*


**臣亮言：先帝创业未半，而中道崩殂；**

Permit Liang to observe: the late Emperor was taken from us before he could finish his life's work, the restoration of the Han.

**今天下三分，益州疲敝，此诚危急存亡之秋也。**

Today, the empire is still divided into three, and our very survival is threatened.

**然侍卫之臣，不懈于内；忠志之士，忘身于外者:**

Yet still the officials at court and the soldiers throughout the realm remain loyal to you, your majesty.

**盖追先帝之殊遇，欲报之于陛下也。**

Because they remember the late Emperor, all of them, and they wish to repay his kindness in service to you.

**诚宜开张圣听，以光先帝遗德，恢弘志士之气；**

This is the moment to extend your divine influence, to honor the memory of the late Emperor and to strengthen the morale of your officers.

**不宜妄自菲薄，引喻失义，以塞忠谏之路也。**

It is not the time to listen to bad advice, or close your ears to the suggestions of loyal men.

……

**亲贤臣，远小人，此先汉所以兴隆也；**

The emperors of Western Han chose their courtiers wisely, and their dynasty flourished.

**亲小人，远贤臣，此后汉所以倾颓也。**

The emperors of Eastern Han chose poorly, and they doomed the empire to ruin.

**先帝在时，每与臣论此事，未尝不叹息痛恨于桓、灵也！**

Whenever the late Emperor discussed the problem with me, he lamented the failings of Emperors Huan and Ling.

**臣本布衣，躬耕于南阳，苟全性命于乱世，不求闻达于诸侯。**

I began as a common man, farming in my fields in Nanyang, doing what I could to survive in an age of chaos. I never had any interest in making a name for myself as a noble.

**先帝不以臣卑鄙，猥自枉屈，三顾臣于草庐之中，咨臣以当世之事**

The late Emperor was not ashamed to visit my cottage and seek my advice.

**由是感激，遂许先帝以驱驰。**

Grateful for his regard, I responded to his appeal and threw myself into his service.

……

**尔来二十有一年矣。先帝知臣谨慎，故临崩寄臣以大事也。**

Now 21 years have past. The late Emperor always appreciated my caution and, in his final days, entrusted me with his cause.

**受命以来，夙夜忧叹，恐付托不效，以伤先帝之明，**

Since that moment, I have been tormented day and night by the fear that I might let him down.

**故五月渡泸，深入不毛。**

That is why I crossed the Lu river at the height of summer, and entered the wastelands beyond.

**今南方已定，兵甲已足，**

Now the south has been subdued, and our forces are fully armed.

**当奖率三军，北定中原，庶竭驽钝，攘除奸凶，兴复汉室，还于旧都。**

I should lead our soldiers to conquer the northern heartland and attempt to remove the hateful traitors, restore the house of Han, and return it to the former capital.

**此臣所以报先帝而忠陛下之职分也。**

This is the way I mean to honor my debt to the late Emperor and fulfill my duty to you.

……

**愿陛下托臣以讨贼兴复之效，**

My only desire is to be permitted to drive out the traitors and restore the Han.

**不效，则治臣之罪，以告先帝之灵。**

If I let you down, punish my offense and report to the spirit of the late Emperor.

……

**陛下亦宜自谋，以咨诹善道，察纳雅言，深追先帝遗诏**，

Your Majesty, consider your course of action carefully. Seek out good advice, and never forget the last words of the late Emperor.

**臣不胜受恩感激。今当远离。**

I depart now on a long expedition, and I will be forever grateful if you heed my advice.

**临表涕零，不知所言。**

Blinded by my own tears, I know not what I write

	注：本文涉及素材来源于网络
module.exports = {
  pathPrefix: `/ehds-blog`,
  siteMetadata: {
    title: `ehds`,
    siteUrl: `https://ehds.gitlab.io/ehds-blog`,
    description: `ehds awesome blog`,
    author: `DongSheng He`,
    authorURL: `https://ehds.gitlab.io/blog`,
    socials: [
      {
        icon: 'github',
        name: 'GitHub',
        url: 'https://github.com/ehds',
      },
      {
        icon: 'twitter',
        name: 'Twitter',
        url: 'https://twitter.com/ds_hale',
      },
      {
        icon: 'weibo',
        name: 'Wibo',
        url: 'https://weibo.com/dshale',
      },
      {
        icon: 'wechat',
        name: 'Wechat',
        url: '',
      },
    ]
  },
  plugins: [
    {
      resolve: `gatsby-theme-ginkgo`,
      options: {
        postPath: "content/posts",
        mdxExtensions: [".mdx", ".md"],
        ga: "UA-137858782-1",
        htmlLang: "zh",
      }
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'REXLNT',
        short_name: 'REXLNT',
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#fff`,
        display: `standalone`,
        icon: "static/favicon.png",
      },
    },
  ]
};
